package configs

import "os"

func envMongoDbConnection() string {
	return os.Getenv("MONGODB_CONNECTION")
}
