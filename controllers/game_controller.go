package controllers

import (
	"context"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"luciens-journey-maker-backend/configs"
	"luciens-journey-maker-backend/models"
	"luciens-journey-maker-backend/responses"
	"net/http"
	"time"
)

var gameCollection = configs.GetCollection(configs.DB, "games")
var validate = validator.New()

func GetGame(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	gameId := c.Params("gameId")

	var game models.Game
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(gameId)
	err := gameCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&game)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(responses.GameResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	return c.Status(http.StatusOK).JSON(responses.GameResponse{Status: http.StatusOK, Message: "success", Data: &fiber.Map{"data": game}})
}

func GetAllGames(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var games []models.Game
	defer cancel()

	results, err := gameCollection.Find(ctx, bson.M{})

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(responses.GameResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	defer results.Close(ctx)
	for results.Next(ctx) {
		var singleGame models.Game
		if err = results.Decode(&singleGame); err != nil {
			return c.Status(http.StatusInternalServerError).JSON(responses.GameResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
		}

		games = append(games, singleGame)
	}

	return c.Status(http.StatusOK).JSON(
		responses.GameResponse{Status: http.StatusOK, Message: "success", Data: &fiber.Map{"data": games}},
	)
}
