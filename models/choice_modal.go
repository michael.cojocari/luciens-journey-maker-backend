package models

type Choice struct {
	Id          string   `json:"id"`
	Description string   `json:"description"`
	Choices     []string `json:"_choices"`
}
