package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Game struct {
	Id      primitive.ObjectID `bson:"_id,omitempty"`
	Name    string             `json:"name"`
	Choices []Choice           `json:"choices"`
}
